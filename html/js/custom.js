jQuery(document).ready(function () {
	
	$('ul.lang li').click(function(){
		$('ul.lang li').removeClass('active');
		$(this).addClass('active');
	});
	
	$('.blc_big_menu .mst').hide();
	
	$('.blc_menu_btn').click(function(){
		$('.blc_big_menu').slideToggle(1600);
		$('ul.lang li').toggleClass('active');
		$('ul.lang li:first-child').toggleClass('active');	
		$('.blc_menu_btn').toggleClass('open');	
		
		if($('#wrapper, .bg_main, #footer').is('.blur')){
			setTimeout(function(){
				$('#wrapper, .bg_main, #footer').removeClass('blur');
			}, 1600);
		}else{
			setTimeout(function(){
				$('#wrapper, .bg_main, #footer').addClass('blur');
			}, 0);			
		}
		
		setTimeout(function(){
			$('.blc_big_menu .mst').show();
			$('.blc_big_menu .mst > li:nth-child(2), .blc_big_menu .mst > li:last-child').addClass('slideInUp');
			$('.blc_big_menu .mst > li:first-child, .blc_big_menu .mst > li:nth-child(3)').addClass('slideInUp');
		}, 400);
		
		if($('.blc_big_menu .mst > li').is('.slideInUp')){
			setTimeout(function(){			
				$('.blc_big_menu .mst > li').stop().slideToggle(1200);			
			}, 100);
		}		
		
		
		setTimeout(function(){
			$('.blc_four_type').slideToggle(800);
		}, 800);		
	});
	
//	$('.blc_four_type .mst_in li .desc_hide').hide();
	
	$('.blc_four_type .mst_in li').mouseover(function(){
		$(this).find('.desc_hide').stop().slideToggle({direction: "up"}, 600);
	});	
	$('.blc_four_type .mst_in li').mouseout(function(){
	  $(this).find('.desc_hide').stop().slideToggle({direction: "up"}, 600);
	});	
	
//-------------------click go top Arrow----------------------------
	// hide #back-top first
	$("#go_top").hide();
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#go_top').fadeIn();
			} else {
				$('#go_top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#go_top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
//-------------------end click go top--------------------------		
	
//	$('.blc_close span').click(function(){
//		$('#wrapper, .bg_main').removeClass('blur');
//		$('.blc_big_menu').slideToggle(600);	
//		$('ul.lang li').removeClass('active');
//		$('ul.lang li:first-child').addClass('active');	
//		$('.blc_menu_btn').removeClass('open');
//	});		
	
//  var topLogo = $('.logo').innerHeight() + 30;
//
//
//      jQuery(window).scroll(function(){
//		if(jQuery(window).scrollTop() >= topLogo){
//			jQuery('.logo_fix').addClass('active'); 
//		} 
//        else {
//        	jQuery('.logo_fix').removeClass('active'); 
//        }  
//	  
//    });

	//$(".fancybox").fancybox();
	
	if($('div select').is('select')){
		$("select").chosen({
			//"disable_search": true, //поле поиска выключено
			"disable_search": false, //поле поиска включено
			width: '100%'
		});
	}
	
	(function($) {
		$(function() {
			$('ul.tabs__caption, .tabs_list').on('click', 'li:not(.active)', function() {
				$(this)
					.addClass('active').siblings().removeClass('active')
					.closest('div.tabs, .tabs_btm').find('div.tabs__content, .tabs_cnt').removeClass('active').eq($(this).index()).addClass('active');
					$('.map_contacts').addClass('active');
					$('.blc_undeground, .blc_list').removeClass('active');
					initialize(map1, 'map10');
			});
		});
	})(jQuery);
	
	
	$('.lbl li:first-child label').click(function(){
		$('.map_contacts').addClass('active');
		$('.blc_undeground, .blc_list').removeClass('active');
	});
	$('.lbl li:nth-child(2) label').click(function(){
		$('.blc_undeground').addClass('active');
		$('.map_contacts, .blc_list').removeClass('active');
	});	
	$('.lbl li:last-child label').click(function(){
		$('.blc_list').addClass('active');
		$('.blc_undeground, .map_contacts').removeClass('active');
	});
	
	
//	var myWidth = 0;
//	myWidth = window.innerWidth;
//	jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	jQuery(window).resize(function(){
//		var myWidth = 0;
//		myWidth = window.innerWidth;
//		jQuery('#size').remove();
//		jQuery('body').prepend('<div id="size" style="background:#000;padding:5px;position:fixed;z-index:99999;color:#fff;">Width = '+myWidth+'</div>');
//	});	
	
	
});



