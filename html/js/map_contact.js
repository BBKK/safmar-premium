	$('#pharmacies .blc_open span').click(function(){
		$(this).toggleClass('active');
		$('#pharmacies .map_contacts').toggleClass('active');
		$('#pharmacies .map-canvas').toggleClass('active');
		var center2 = map2.getCenter();
		map2.setCenter(center2);
		initialize(map1, 'map10');		
	})
	$('#clinics .blc_open span').click(function(){
		$(this).toggleClass('active');
		$('#clinics .map_contacts').toggleClass('active');
		$('#clinics .map-canvas').toggleClass('active');
		var center = map1.getCenter();
		map1.setCenter(center);
		initialize(map2, 'map20');
	})	
	
	
	var map1 = 0, map2;

	jQuery('#tabs li a').click(function (e) {
		e.preventDefault();
		jQuery(this).tab('show')
		if(jQuery(this).attr('href')=='#pharmacies'){
			var center2 = map2.getCenter();
			map2.setCenter(center2);
			initialize(map1, 'map10');
		}
		if(jQuery(this).attr('href')=='#clinics'){
			var center = map1.getCenter();
			map1.setCenter(center);
			initialize(map2, 'map20');
		}	

	});


	jQuery(function($) {
		initialize(map1, 'map10');
		initialize(map2, 'map20');
	});
	

        function initialize(map, mapName) {    
                 var my_map = 'custom_style';
                 //var bounds = new google.maps.LatLngBounds();
                 var featureOpts = [
					{"featureType":"administrative","elementType":"labels.text.fill","stylers":[
						{"color":"#444444"}]},{"featureType": "landscape","elementType": "geometry","stylers": [{"color": "#f2f2f2"}]},{"featureType": "landscape","elementType": "geometry.fill","stylers": [{"color": "#f2f2f2"}]},{"featureType": "landscape","elementType": "geometry.stroke","stylers": [{"color": "#a7a7a7" }]}, {"featureType": "landscape","elementType": "labels","stylers": [{"color": "#ff0000"}]}, {"featureType": "landscape","elementType": "labels.text","stylers": [{"color": "#ff0000"}]},{"featureType": "landscape","elementType": "labels.text.fill","stylers": [{"color": "#ff0000"}]},{"featureType": "landscape","elementType": "labels.text.stroke", "stylers": [{"color": "#ff0000" }]},{ "featureType": "landscape","elementType": "labels.icon","stylers": [{"color": "#ff0000"}] },{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#dbdbdb"},{"visibility":"on"}]}]
          

                $('body').find('#' + mapName).each(function(){
                        var panByX = parseInt($(this).parent().find('.map-canvas').attr('panByX'));
                        var panByY = parseInt($(this).parent().find('.map-canvas').attr('panByY'));
                        var startx = parseInt($(this).parent().find('.map-canvas').attr('startx'));
                        var starty = parseInt($(this).parent().find('.map-canvas').attr('starty'));
                        var zoom = parseInt($(this).parent().find('.map-canvas').attr('zoom'));
                        var comment = '';

                        if(!(zoom>0)){
                                zoom = 12;
                        }
                        if((startx>0)&&(starty>0)){
                                var Moscow = new google.maps.LatLng($(this).parent().find('.map-canvas').attr('starty'), $(this).parent().find('.map-canvas').attr('startx'));
                        }else{
                                var Moscow = new google.maps.LatLng($(this).parent().find('span:first').attr('y'), $(this).parent().find('span:first').attr('x'));
                        }
                        var mapOptions = {
                                center: Moscow,
                                panControl: true,
                                scaleControl:true,
                                streetViewControl: true,
                                overviewMapControl: true,
                                rotateControl:true,
                                zoom: zoom,
                                zoomControl: true,
                                zoomControlOptions: {
                                  style:google.maps.ZoomControlStyle.DEFAULT
                                },                                                             
                                mapTypeControl: false,
                                scrollwheel: false,
                                mapTypeControlOptions: {
                                  mapTypeIds: [google.maps.MapTypeId.ROADMAP, my_map]
                                },
                                mapTypeId: my_map
                        }; // end mapOptions;
                        
                        var map = new google.maps.Map(document.getElementById($(this).attr('id')), mapOptions);
					
                        if(mapName == 'map10'){
                            map1 = map;
                        }
                        if(mapName == 'map20'){
                            map2 = map;
                        }					

                        if((panByX + panByY)>0){
                                map.panBy(panByX,panByY);// maps shift center
                        }

                        var ob = [];
                        var i = 0;
                        $(this).parent().find('span').each(function(index, elem){
                                var type = $(this).attr('type');
                                if(type==''){
                                        type = 'non';
                                }
                                var name = $(this).attr('name');
                                var title = name;
                                if(name!=''){
                                        name = '<h5>'+name+'</h5>';
                                }
                                var text = $(this).attr('text');
                                if(text!=''){
                                        text = '<p class="text">'+text+'</p>';
                                }
                                var url = $(this).attr('url');
                                if(url!=''){
                                        url = '<p class="url"><a target="_blank" href="http://'+url+'">'+url+'</p>';
                                }
                                var city = $(this).attr('city');
                                var adress = $(this).attr('adress');
                                if((city!='')&&(adress!='')){
                                        adress = '<p class="adr">'+city+', '+adress+'</p>';
                                        city = '<p class="adr">'+city+'</p>';
                                }else if((city!='')){
                                        adress = '<p class="adr">'+city+'</p>'; 
                                        city = '<p class="adr">'+city+'</p>';
                                }else if((adress!='')){
                                        adress = '<p class="adr">'+adress+'</p>'; 
                                }
                                var phone = $(this).attr('phone');
                                if(phone!=''){
                                        phone = '<p class="phone"><a href="tel:'+phone+'">'+phone+'</a></p>';
                                }
                                var phone2 = $(this).attr('phone2');
                                if(phone2!=''){
                                        phone2 = '<p class="phone"><a href="tel:'+phone2+'">'+phone2+'</a></p>';
                                }
                                var email = $(this).attr('email');
                                if(email!=''){
                                        email = '<p class="email"><a href="mailto: '+email+'">'+email+'</a></p>';
                                }
                                var time = $(this).attr('time');
                                if(time!=''){
                                        time = '<p class="time">'+time+'</p>';
                                }
                                var metro = $(this).attr('metro');
                                if(metro!=''){
                                        metro = '<p class="metro">'+metro+'</p>';
                                }

                                
                                ob[i] = {
                                        ele: String(i),
                                        name: name,
                                        title: title,
                                        text: text,
                                        url: url,
                                        x: $(this).attr('x'),
                                        y: $(this).attr('y'),
                                        city: city,
                                        adress: adress,
                                        phone: phone,
                                        phone2: phone2,
                                        email: email,
                                        time: time,
                                        metro: metro
                                        };
                                i = i+1;

                        });
                        var count_element = i;
                        map.setTilt(45);
                        var markers = [];


                        // Display multiple markers on a map
                        var infoWindow = new google.maps.InfoWindow(), marker, i;

                        // Loop through our array of markers & place each one on the map  
                        for( i = 0; i < markers.length; i++ ) {
                                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                                //bounds.extend(position);
                                      marker = new google.maps.Marker({
                                              position: position,
                                              map: map,
                                              title: markers[i][0]
                                      });
                                // Allow each marker to have an info window    
                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                    return function() {
                                        infoWindow.setContent(infoWindowContent[i][0]);
                                        infoWindow.open(map, marker);
                                        }
                                })(marker, i));  
                        } 
                        var styledMapOptions = {name: 'Custom Style'};
                        var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

                        map.mapTypes.set(my_map, customMapType);
                        var j = 0;
                        var infoWindowContentsOffice = [];

                        while(j<count_element){
                                //console.log(marker[ob[j]['type']]);
                                
                                var officeImage = new google.maps.MarkerImage('images/marker_map.png',
                                        new google.maps.Size(27,38),
                                        new google.maps.Point(0,0),
                                        new google.maps.Point(27,38)
                                );

                                var officeShadow = new google.maps.MarkerImage('images/marker_map.png',
                                        new google.maps.Size(27,38),
                                        new google.maps.Point(0,0),
                                        new google.maps.Point(27,38));

                                var office = [
                                        [ob[j]['ele'], ob[j]['y'], ob[j]['x']]
                                ];
                                infoWindowContentsOffice[j] = [
                                        ['<div class="info_content">'+ob[j]['name']+ob[j]['adress']+ob[j]['metro']+ob[j]['phone']+ob[j]['phone2']+ob[j]['email']+ob[j]['time']+ob[j]['url']+'<p class="comment_balun_map">'+comment+'</p></div>']
                                ];
                                
                                var infoOffice = new google.maps.InfoWindow(), office, i;
                                var position = new google.maps.LatLng(office[i][1], office[i][2]);

                                office = new google.maps.Marker({
                                        position: position,
                                        map: map,
                                        icon: officeImage,
                                        shadow: officeShadow,
                                        optimized: false,
                                        zIndex:9,
                                        title: ob[j]['title'],
                                        id: office[i][0]
                                });
                                //console.log(infoWindowContentsOffice);
                                
                                google.maps.event.addListener(office, 'click', (function(office, i, j) {
                                        return function() {
                                                infoOffice.setContent(infoWindowContentsOffice[office.id][i][0]);
                                                infoOffice.open(map, office);
                                        };
                                })(office, i));
                                j = j+1;
                        };
                });
        } // end initialize()